// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'ion-gallery', 'ngCordova', 'ionic.contrib.drawer','tabSlideBox','ngCordovaOauth'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
  if (window.cordova && window.cordova.plugins.Keyboard) {
    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    cordova.plugins.Keyboard.disableScroll(true);

  }
  if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {

  $httpProvider.defaults.headers.common = {};
  $httpProvider.defaults.headers.post = {};
  $httpProvider.defaults.headers.put = {};
  $httpProvider.defaults.headers.patch = {};

  
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
 
  .state('app.index', {
    url: '/index',
    views: {
      'menuContent': {
        templateUrl: 'templates/index.html',
        controller: 'IndexCtrl'
      }
    }
  })
  .state('app.registro', {
    url: '/registro',
    views: {
      'menuContent': {
        templateUrl: 'templates/registro.html',
        controller: 'RegistroCtrl'
      }
    }
  })
  .state('app.gracias', {
    url: '/gracias',
    views: {
      'menuContent': {
        templateUrl: 'templates/gracias.html',
        controller: 'GraciasCtrl'
      }
    }
  })
  .state('app.intereses', {
    url: '/intereses',
    views: {
      'menuContent': {
        templateUrl: 'templates/intereses.html',
        controller: 'InteresesCtrl'
      }
    }
  })
  .state('app.otrosIntereses', {
    url: '/otrosIntereses',
    views: {
      'menuContent': {
        templateUrl: 'templates/otrosIntereses.html',
        controller: 'OtrosInteresesCtrl'
      }
    }
  })
  .state('app.localizar', {
    url: '/localizar',
    views: {
      'menuContent': {
        templateUrl: 'templates/localizar.html',
        controller: 'LocalizarCtrl'
      }
    }
  })
  .state('app.info', {
    url: '/info',
    views: {
      'menuContent': {
        templateUrl: 'templates/info.html',
        controller: 'InfoCtrl'
      }
    }
  })
  .state('app.descubre', {
    url: '/descubre',
    views: {
      'menuContent': {
        templateUrl: 'templates/descubre.html',
        controller: 'DescubreCtrl'
      }
    }
  })
  .state('app.aprende', {
    url: '/aprende',
    views: {
      'menuContent': {
        templateUrl: 'templates/aprende.html',
        controller: 'AprendeCtrl'
      }
    }
  })
  .state('app.compra', {
    url: '/compra',
    views: {
      'menuContent': {
        templateUrl: 'templates/compra.html',
        controller: 'CompraCtrl'
      }
    }
  })
  .state('app.vive', {
    url: '/vive',
    views: {
      'menuContent': {
        templateUrl: 'templates/vive.html',
        controller: 'ViveCtrl'
      }
    }
  })
  .state('app.comentar', {
    url: '/comentar',
    views: {
      'menuContent': {
        templateUrl: 'templates/comentar.html',
        controller: 'ComentarCtrl'
      }
    }
  })
  .state('app.ayudanos', {
    url: '/ayudanos',
    views: {
      'menuContent': {
        templateUrl: 'templates/ayudanos.html',
        controller: 'AyudanosCtrl'
      }
    }
  })
  .state('app.mapa', {
    url: '/mapa',
    views: {
      'menuContent': {
        templateUrl: 'templates/map.html',
        controller: 'MapaCtrl'
      }
    }
  })
   .state('app.mapafiltro', {
    url: '/mapaFiltro',
    views: {
      'menuContent': {
        templateUrl: 'templates/mapaFiltro.html',
        controller: 'MapaCtrl'
      }
    }
  })
  .state('app.maparaymi', {
    url: '/mapaRaymi',
    views: {
      'menuContent': {
        templateUrl: 'templates/mapaRaymi.html',
        controller: 'MapaCtrl'
      }
    }
  })
  .state('app.descubreCat', {
    url: '/descubreCat/:catId',
    views: {
      'menuContent': {
        templateUrl: 'templates/descubreCat.html',
        controller: 'DescubreCatCtrl'
      }
    }
  })
  .state('app.galeria', {
    url: '/galeria/:galeriaId',
    views: {
      'menuContent': {
        templateUrl: 'templates/galeria.html',
        controller: 'GaleriaCtrl'
      }
    }
  }) 
 
  .state('app.item', {
    url: '/item',
    views: {
      'menuContent': {
        templateUrl: 'templates/item.html',
        controller: 'ItemCtrl'
      }
    }
  })
  .state('app.raymi', {
    url: '/raymi',
    views: {
      'menuContent': {
        templateUrl: 'templates/raymi.html',
        controller: 'RaymiCtrl'
      }
    }
  })
  .state('app.search', {
    url: '/search/:word',      
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
        controller: 'SearchCtrl'
      }
    }
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/index');
})
.config(function(ionGalleryConfigProvider) {
  ionGalleryConfigProvider.setGalleryConfig({
    action_label: 'Close',
    toggle: false,
    row_size: 3,
    fixed_row_size: true
  });
})

.directive('myOnKeyDownCall', function ($rootScope) {
  return function (scope, element, attrs) {
   var numKeysPress=0;
   element.bind("keyup", function (event) {   
     numKeysPress++;
     if(numKeysPress>=3){
      scope.$apply(function (){
        scope.$eval(attrs.myOnKeyDownCall);
                   // console.log(element.val());                    
                 });
              // event.preventDefault();

              if(event.which === 13) {
                scope.$apply(function(){
                  scope.$eval(attrs.ngEnter, {'event': event});                                        

                  $rootScope.busca = element.val() || "";

                });

              }
            }
          })
 }
})
.directive( 'elemReady', function( $parse ) {
 return {
   restrict: 'A',
   link: function( $scope, elem, attrs ) {    
    elem.ready(function(){
      $scope.$apply(function(){
        var func = $parse(attrs.elemReady);
        func($scope);
      })
    })
  }
}
})
.filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                  //Also remove . and , so its gives a cleaner result.
                  if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
                    lastspace = lastspace - 1;
                  }
                  value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    })
 .filter('split', function() {
        return function(input, splitChar, splitIndex) {
            // do some bounds checking here to ensure it has that index
            return input.split(splitChar)[splitIndex];
        }
    });


;

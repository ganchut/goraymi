angular.module('starter.controllers', [])

.controller('AppCtrl',['$scope','$ionicModal','$timeout','$state','$rootScope', '$http','$filter', function($scope, $ionicModal, $timeout, $state, $rootScope, $http, $filter) { 

  // Variables globales
  $scope.loginData = {};
  $scope.arguments={};
  $scope.data={};
  $rootScope.app = {};
  $rootScope.data = {};
  

  if(localStorage.getItem("token") !== null && localStorage.getItem("token") !== ""){console.log("hay un token guardado :)");$state.go('app.info');}


  $scope.goItemFromCat = function(cat,index){


  var categoryJSON = JSON.stringify({"data": {"keyword": index,"page":0},"option": "Item","action": "categorie","token":localStorage.getItem("token")});

  var encrypted = CryptoJS.AES.encrypt(categoryJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
  var encryptedText = encrypted.toString();

  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://goraymidev.softonthecloud.net/ws/",
    "method": "POST",
    "headers": {   
      "content-type": "application/x-www-form-urlencoded"
    },
    "data": {
      "data": encryptedText
    }
  }

  $.ajax(settings).done(function (response) {
    key = CryptoJS.enc.Utf8.parse('1234567891234567');  
    ciphertext = response; 
    console.log(response.length);
    var options = {mode: CryptoJS.mode.ECB};  
    plaintext = CryptoJS.AES.decrypt(ciphertext, key,options); 
    var data = CryptoJS.enc.Utf8.stringify(plaintext);  

    data = data.replace(/\\n/g, "\\n")  
    .replace(/\\'/g, "\\'")
    .replace(/\\"/g, '\\"')
    .replace(/\\&/g, "\\&")
    .replace(/\\r/g, "\\r")
    .replace(/\\t/g, "\\t")
    .replace(/\\b/g, "\\b")
    .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
data = data.replace(/[\u0000-\u0019]+/g,""); 

var o = JSON.parse(data);
$scope.data.catItems = o.data.data;
})
  .error(function(e){
    console.log(e);
  }); 


  if(cat == 1){
    $scope.color = "purple";
  }
  else if (cat == 2){
    $scope.color = "blue";
  }
  else if (cat == 3){
    $scope.color = "orange";
  }
  else {
    $scope.color = "green";
  }
      $state.go('app.descubreCat');

};

$scope.viewItemCat = function(color,index){
   $scope.arguments.itemView = $scope.data.catItems.results[index];
    $state.go('app.item');
} 

$scope.filtroTodo = function (){
  $scope.data.filtro = angular.copy($scope.data.home);
   
};
$scope.filtroAlfabetico = function(){
 $scope.data.filtro.items.buy = $filter('orderBy')($scope.data.filtro.items.buy,'language_item_title');
 $scope.data.filtro.items.learn = $filter('orderBy')($scope.data.filtro.items.learn,'language_item_title');
 $scope.data.filtro.items.discover = $filter('orderBy')($scope.data.filtro.items.discover,'language_item_title');
 $scope.data.filtro.items.live = $filter('orderBy')($scope.data.filtro.items.live,'language_item_title');
};
$scope.filtroOficial = function(){ 
 $scope.data.filtro.items.buy = $filter('filter')($scope.data.filtro.items.buy,{item_official: "1"});
 $scope.data.filtro.items.learn = $filter('filter')($scope.data.filtro.items.learn,{item_official: "1"});
 $scope.data.filtro.items.discover = $filter('filter')($scope.data.filtro.items.discover,{item_official: "1"});
 $scope.data.filtro.items.live = $filter('filter')($scope.data.filtro.items.live,{item_official: "1"});
}
$scope.filtroCosto = function(){ 
 $scope.data.filtro.items.buy = $filter('orderBy')($scope.data.filtro.items.buy,'item_price_pp');
 $scope.data.filtro.items.learn = $filter('orderBy')($scope.data.filtro.items.learn,'item_price_pp');
 $scope.data.filtro.items.discover = $filter('orderBy')($scope.data.filtro.items.discover,'item_price_pp');
 $scope.data.filtro.items.live = $filter('orderBy')($scope.data.filtro.items.live,'item_price_pp');
}

//$scope.ObtenerToken = function (){
  var obtenerToken = JSON.stringify({"data": {"user": "adminApp","password": "McomSoft2013"},"option": "WebService","action": "logIn"});

  var encrypted = CryptoJS.AES.encrypt(obtenerToken, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
  var encryptedText = encrypted.toString();



  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://goraymidev.softonthecloud.net/ws/",
    "method": "POST",
    "headers": {   
      "content-type": "application/x-www-form-urlencoded"
    },
    "data": {
      "data": encryptedText
    }
  }

  $.ajax(settings).done(function (response) {
    key = CryptoJS.enc.Utf8.parse('1234567891234567');  
    ciphertext = response; 
    var options = {mode: CryptoJS.mode.ECB};  
    plaintext = CryptoJS.AES.decrypt(ciphertext, key,options); 
    var data = CryptoJS.enc.Utf8.stringify(plaintext);  


    data = data.replace(/\\n/g, "\\n")  
    .replace(/\\'/g, "\\'")
    .replace(/\\"/g, '\\"')
    .replace(/\\&/g, "\\&")
    .replace(/\\r/g, "\\r")
    .replace(/\\t/g, "\\t")
    .replace(/\\b/g, "\\b")
    .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
data = data.replace(/[\u0000-\u0019]+/g,""); 
var o = JSON.parse(data);
$rootScope.app.token=o.data.token;
localStorage.setItem("token", $rootScope.app.token);
});

//};

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
   // console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
}])

.controller('BackButtonCtrl',function($scope, $ionicHistory) {

  $scope.GoBack = function() {
    $ionicHistory.goBack();    
  };
})

.controller('SearchCtrl',['$scope', '$state','$rootScope',function($scope, $state){

  $scope.$watch('busca', function (value) {
    if(value)
      if(value.length>=3){

      
   var buscaJSON = JSON.stringify({"data": {"search": value},"option": "Item","action": "getBoxSearch","token":localStorage.getItem("token")});

   var encrypted = CryptoJS.AES.encrypt(buscaJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
   var encryptedText = encrypted.toString();

   var settings = {
    "async": false,
    "crossDomain": true,
    "url": "http://goraymidev.softonthecloud.net/ws/",
    "method": "POST",
    "headers": {   
      "content-type": "application/x-www-form-urlencoded"
    },
    "data": {
      "data": encryptedText
    }
  }

  $.ajax(settings).done(function (response) {
    key = CryptoJS.enc.Utf8.parse('1234567891234567');  
    ciphertext = response; 
    var options = {mode: CryptoJS.mode.ECB};  
    plaintext = CryptoJS.AES.decrypt(ciphertext, key,options); 
    var data = CryptoJS.enc.Utf8.stringify(plaintext);  


    data = data.replace(/\\n/g, "\\n")  
    .replace(/\\'/g, "\\'")
    .replace(/\\"/g, '\\"')
    .replace(/\\&/g, "\\&")
    .replace(/\\r/g, "\\r")
    .replace(/\\t/g, "\\t")
    .replace(/\\b/g, "\\b")
    .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
data = data.replace(/[\u0000-\u0019]+/g,""); 
$scope.data.search = JSON.parse(data).data;   
//console.log(o.data);

});

    $state.go('app.search');
      }
    },true);  


$scope.viewItem = function(cat,index){

  if(cat == 1){
    $scope.arguments.itemView = $scope.data.search.discover[index];
    $state.go('app.item');


  }
  else if (cat == 2){
      $scope.arguments.itemView = $scope.data.search.learn[index];
      $state.go('app.item');
  }
  else if (cat == 3){
      $scope.arguments.itemView = $scope.data.search.buy[index];
      $state.go('app.item');
  }
  else {
      $scope.arguments.itemView = $scope.data.search.live[index];
      $state.go('app.item');
  }
}
}])
.controller('InfoCtrl', function($scope,$state,$rootScope,$ionicLoading) {  
  $ionicLoading.show({
    template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Cargando contenido'});
  var userData =  JSON.parse(localStorage.getItem("userData"));
  if(userData){    
        var homeJSON = JSON.stringify({"data": {"lang": userData.data.user[0].user_default_language},"option": "Item","action": "getHomeItems","token":localStorage.getItem("token")});
   }else {
          var homeJSON = JSON.stringify({"data": {"lang": "ES_es"},"option": "Item","action": "getHomeItems","token":localStorage.getItem("token")});
      }
      var categoriesJSON =  JSON.stringify({"data": {"lang": "ES_es"},"option": "Item","action": "getAppCategoriesTree","token":localStorage.getItem("token")});

       var encrypted = CryptoJS.AES.encrypt(homeJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
       var categoriesEncrypted = CryptoJS.AES.encrypt(categoriesJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
       var encryptedText = encrypted.toString();
       var categoriesEncryptedText = categoriesEncrypted.toString();

       var settings = {
        "async": false,
        "crossDomain": true,
        "url": "http://goraymidev.softonthecloud.net/ws/",
        "method": "POST",
        "headers": {   
          "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
          "data": encryptedText
        }
      }

       var categoriesSettings = {
        "async": false,
        "crossDomain": true,
        "url": "http://goraymidev.softonthecloud.net/ws/",
        "method": "POST",
        "headers": {   
          "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
          "data": categoriesEncryptedText
        }
      }

      $.ajax(settings).done(function (response) {
        key = CryptoJS.enc.Utf8.parse('1234567891234567');  
        ciphertext = response; 
        var options = {mode: CryptoJS.mode.ECB};  
        plaintext = CryptoJS.AES.decrypt(ciphertext, key,options);        
        var data = CryptoJS.enc.Utf8.stringify(plaintext);  

        data = data.replace(/\\n/g, "\\n")  
        .replace(/\\'/g, "\\'")
        .replace(/\\"/g, '\\"')
        .replace(/\\&/g, "\\&")
        .replace(/\\r/g, "\\r")
        .replace(/\\t/g, "\\t")
        .replace(/\\b/g, "\\b")
        .replace(/\\f/g, "\\f");
  // remove non-printable and other non-valid JSON chars
  data = data.replace(/[\u0000-\u0019]+/g,""); 
  var o = JSON.parse(data); 
  $scope.data.home = o.data;  
  $rootScope.data.filtro = angular.copy(o.data);
  $scope.data.filtro = angular.copy(o.data);
});

 $.ajax(categoriesSettings).done(function (response) {
        key = CryptoJS.enc.Utf8.parse('1234567891234567');  
        ciphertext = response; 
        var options = {mode: CryptoJS.mode.ECB};  
        plaintext = CryptoJS.AES.decrypt(ciphertext, key,options);        
        var data = CryptoJS.enc.Utf8.stringify(plaintext);  

        data = data.replace(/\\n/g, "\\n")  
        .replace(/\\'/g, "\\'")
        .replace(/\\"/g, '\\"')
        .replace(/\\&/g, "\\&")
        .replace(/\\r/g, "\\r")
        .replace(/\\t/g, "\\t")
        .replace(/\\b/g, "\\b")
        .replace(/\\f/g, "\\f");
  // remove non-printable and other non-valid JSON chars
  data = data.replace(/[\u0000-\u0019]+/g,""); 
  var o = JSON.parse(data); 
  $scope.data.categories = o.data; 

  $ionicLoading.hide();
});



  $scope.openMenu = function(){
    var myEl = angular.element( document.querySelector( '#menuUser' ) );
    myEl.addClass('abrirMenu');
    myEl.removeClass('cerrarMenu');
  };

  $scope.closeDrawer2 = function(){
    var myEl = angular.element( document.querySelector( '#menuUser' ) );
    myEl.addClass('cerrarMenu');
    myEl.removeClass('abrirMenu');
  } ;


  $scope.viewItem = function(cat,index){
  if(cat == 1){
    $scope.arguments.itemView = $scope.data.home.items.discover[index];
    $state.go('app.item');


  }
  else if (cat == 2){
      $scope.arguments.itemView = $scope.data.home.items.learn[index];
      $state.go('app.item');
  }
  else if (cat == 3){
      $scope.arguments.itemView = $scope.data.home.items.buy[index];
      $state.go('app.item');
  }
  else {
      $scope.arguments.itemView = $scope.data.home.items.live[index];
      $state.go('app.item');
  }
};

$scope.viewRaymi = function(cat,hash,user){     

 $ionicLoading.show({
    template: '<ion-spinner icon="bubbles"></ion-spinner><br/>'
  });
       var raymiJSON = JSON.stringify({"data": {"language":"ES_es","hash":hash,"user_id":user},"option": "Item","action": "getRaymiChildren","token":localStorage.getItem("token")});
       var encrypted = CryptoJS.AES.encrypt(raymiJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
       var encryptedText = encrypted.toString();

       var settings = {
        "async": false,
        "crossDomain": true,
        "url": "http://goraymidev.softonthecloud.net/ws/",
        "method": "POST",
        "headers": {   
          "content-type": "application/x-www-form-urlencoded"
        },
        "data": {
          "data": encryptedText
        }
      }

      $.ajax(settings).done(function (response) {
        key = CryptoJS.enc.Utf8.parse('1234567891234567');  
        ciphertext = response; 
        var options = {mode: CryptoJS.mode.ECB};  
        plaintext = CryptoJS.AES.decrypt(ciphertext, key,options); 
        var data = CryptoJS.enc.Utf8.stringify(plaintext);  

        data = data.replace(/\\n/g, "\\n")  
        .replace(/\\'/g, "\\'")
        .replace(/\\"/g, '\\"')
        .replace(/\\&/g, "\\&")
        .replace(/\\r/g, "\\r")
        .replace(/\\t/g, "\\t")
        .replace(/\\b/g, "\\b")
        .replace(/\\f/g, "\\f");
  // remove non-printable and other non-valid JSON chars
  data = data.replace(/[\u0000-\u0019]+/g,""); 
  var o = JSON.parse(data);   

  $scope.data.raymiView = o.data.link;
  console.log($scope.data.raymiView);
  $state.go('app.raymi'); 
  $ionicLoading.hide();  
  })



}
})


.controller('IndexCtrl', function($scope,$ionicPopup,$state, $cordovaOauth, $location, $ionicLoading, $http) { 

  $scope.login = function() {   
   var loginJSON = JSON.stringify({"data": {"user_email": $scope.loginData.username,"user_psw": $scope.loginData.password},"option": "User","action": "logIn","token":localStorage.getItem("token")});

   var encrypted = CryptoJS.AES.encrypt(loginJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
   var encryptedText = encrypted.toString();

   var settings = {
    "async": false,
    "crossDomain": true,
    "url": "http://goraymidev.softonthecloud.net/ws/",
    "method": "POST",
    "headers": {   
      "content-type": "application/x-www-form-urlencoded"
    },
    "data": {
      "data": encryptedText
    }
  }

  $.ajax(settings).done(function (response) {
    key = CryptoJS.enc.Utf8.parse('1234567891234567');  
    ciphertext = response; 
    var options = {mode: CryptoJS.mode.ECB};  
    plaintext = CryptoJS.AES.decrypt(ciphertext, key,options); 
    var data = CryptoJS.enc.Utf8.stringify(plaintext);  


    data = data.replace(/\\n/g, "\\n")  
    .replace(/\\'/g, "\\'")
    .replace(/\\"/g, '\\"')
    .replace(/\\&/g, "\\&")
    .replace(/\\r/g, "\\r")
    .replace(/\\t/g, "\\t")
    .replace(/\\b/g, "\\b")
    .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
data = data.replace(/[\u0000-\u0019]+/g,""); 
var o = JSON.parse(data);   
//console.log(o);

if(o.Error ===1){
  $ionicPopup.alert({
   title: 'Error',
   template: 'El correo y/o la contraseña no son válidos'

 });
}else if(o.Error ===0){
  $state.go("app.info");
}
else {
 $ionicPopup.alert({
   title: 'Error',
   template: 'Parece que ahora mismo tenemos problemas, intentalo más tarde'

 });
}

});

}
   $scope.facebookSignIn = function(){
        $ionicLoading.show({template: 'Iniciando sesión...'});
        $cordovaOauth.facebook("621383558029492", ["email"]).then(function(result) {
            localStorage.accessToken = result.access_token;   
             if(result.access_token) {
              $http.get("https://graph.facebook.com/v2.2/me", { params: { access_token: localStorage.accessToken, fields: "id,name,gender,location,website,picture,relationship_status, email", format: "json" }}).then(function(result) {

        var registroJSON = JSON.stringify({"data":{"user": {"user_facebook_id": result.data.id}},"option": "User","action": "register","token":localStorage.getItem("token")});

        var encrypted = CryptoJS.AES.encrypt(registroJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
        var encryptedText = encrypted.toString();

        var settings = {
          "async": false,
          "crossDomain": true,
          "url": "http://goraymidev.softonthecloud.net/ws/",
          "method": "POST",
          "headers": {   
            "content-type": "application/x-www-form-urlencoded"
          },
          "data": {
            "data": encryptedText
          }
        }

        $.ajax(settings).done(function (response) {
          key = CryptoJS.enc.Utf8.parse('1234567891234567');  
          ciphertext = response; 
          var options = {mode: CryptoJS.mode.ECB};  
          plaintext = CryptoJS.AES.decrypt(ciphertext, key,options); 
          var data = CryptoJS.enc.Utf8.stringify(plaintext);  


          data = data.replace(/\\n/g, "\\n")  
          .replace(/\\'/g, "\\'")
          .replace(/\\"/g, '\\"')
          .replace(/\\&/g, "\\&")
          .replace(/\\r/g, "\\r")
          .replace(/\\t/g, "\\t")
          .replace(/\\b/g, "\\b")
          .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
data = data.replace(/[\u0000-\u0019]+/g,""); 
var o = JSON.parse(data);
 
if(o.Error ===0){
   localStorage.setItem("userData", JSON.stringify(o));   
      $state.go("app.localizar");
  }
     else if(o.Error ===1 && o.ErrorMessage === "user_facebook_id allready in use"){
      $state.go("app.info");

     } else {
      $ionicPopup.alert({
       title: 'Error',
       template: 'Ha habido algún error al registrate, por favor, inténtalo más tarde'   
     });
    }
  });


                  $ionicLoading.hide();                 
              }, function(error) {
                  alert("There was a problem getting your profile.  Check the logs for details.");
                  console.log(error);
              });
          } else {
              alert("Not signed in");
              $location.path("/login");
          }
           
            //$state.go("app.localizar");
        }, function(error) {
            $ionicLoading.hide();
            console.log(error);
        })
    };

    $scope.twitterSignIn = function(){
       // var api_key = "CeDoBEcgTW9542B0IRVtudnn3"; //Enter your Consumer Key (API Key)
       // var api_secret = "oGSMKtuPJK1mtoaEWK654O89TSHbsgC2nHuCIPHTFSIugw3O4M"; // Enter your Consumer Secret (API Secret)
        $ionicLoading.show({template: 'Iniciando sesión...'});
         $cordovaOauth.twitter("CeDoBEcgTW9542B0IRVtudnn3", "oGSMKtuPJK1mtoaEWK654O89TSHbsgC2nHuCIPHTFSIugw3O4M").then(function(result) {
          var registroJSON = JSON.stringify({"data":{"user": {"user_twitter_id": result.user_id}},"option": "User","action": "register","token":localStorage.getItem("token")});

           var encrypted = CryptoJS.AES.encrypt(registroJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
        var encryptedText = encrypted.toString();

        var settings = {
          "async": false,
          "crossDomain": true,
          "url": "http://goraymidev.softonthecloud.net/ws/",
          "method": "POST",
          "headers": {   
            "content-type": "application/x-www-form-urlencoded"
          },
          "data": {
            "data": encryptedText
          }
        };


          $.ajax(settings).done(function (response) {
            key = CryptoJS.enc.Utf8.parse('1234567891234567');  
            ciphertext = response; 
            var options = {mode: CryptoJS.mode.ECB};  
            plaintext = CryptoJS.AES.decrypt(ciphertext, key,options); 
            var data = CryptoJS.enc.Utf8.stringify(plaintext);  


            data = data.replace(/\\n/g, "\\n")  
            .replace(/\\'/g, "\\'")
            .replace(/\\"/g, '\\"')
            .replace(/\\&/g, "\\&")
            .replace(/\\r/g, "\\r")
            .replace(/\\t/g, "\\t")
            .replace(/\\b/g, "\\b")
            .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
data = data.replace(/[\u0000-\u0019]+/g,""); 
var o = JSON.parse(data);   
if(o.Error ===0){
  $ionicLoading.hide();
   localStorage.setItem("userData", JSON.stringify(o));   
    $state.go("app.localizar");
  }
  else if(o.Error ===1){
    $ionicLoading.hide();
    $state.go("app.info");    
  }

})
        }), function(error){
            $ionicLoading.hide();
            console.log(error);
        }
  };

})
.controller('RegistroCtrl', function($scope, $state,$ionicPopup,$rootScope) {
  $scope.registroForm = {};

  $scope.registro = function(registroForm){   

    if(registroForm.$valid){
      if(registroForm.password == registroForm.repitePassword)
      {

        var registroJSON = JSON.stringify({"data":{"user": {"user_email": registroForm.email,"user_psw": registroForm.password}},"option": "User","action": "register","token":localStorage.getItem("token")});

        var encrypted = CryptoJS.AES.encrypt(registroJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
        var encryptedText = encrypted.toString();

        var settings = {
          "async": false,
          "crossDomain": true,
          "url": "http://goraymidev.softonthecloud.net/ws/",
          "method": "POST",
          "headers": {   
            "content-type": "application/x-www-form-urlencoded"
          },
          "data": {
            "data": encryptedText
          }
        }

        $.ajax(settings).done(function (response) {
          key = CryptoJS.enc.Utf8.parse('1234567891234567');  
          ciphertext = response; 
          var options = {mode: CryptoJS.mode.ECB};  
          plaintext = CryptoJS.AES.decrypt(ciphertext, key,options); 
          var data = CryptoJS.enc.Utf8.stringify(plaintext);  


          data = data.replace(/\\n/g, "\\n")  
          .replace(/\\'/g, "\\'")
          .replace(/\\"/g, '\\"')
          .replace(/\\&/g, "\\&")
          .replace(/\\r/g, "\\r")
          .replace(/\\t/g, "\\t")
          .replace(/\\b/g, "\\b")
          .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
data = data.replace(/[\u0000-\u0019]+/g,""); 
var o = JSON.parse(data);

if(o.Error ===0){
       // console.log(o.data.user[0].user_id);        
       $rootScope.app.user = o;
       localStorage.setItem("userData", JSON.stringify($rootScope.app.user));

       $state.go("app.gracias");

     }else if(o.Error ===1 && o.ErrorMessage === "user_email allready in use"){
       $ionicPopup.alert({
         title: 'Error',
         template: 'Este usuario ya está en uso.'

       });

     } else {
      console.log(o);
      $ionicPopup.alert({
       title: 'Error',
       template: 'Ha habido algún error al registrate, por favor, inténtalo más tarde'   
     });
    }
  });
}

}
else{
  registroForm.$invalid;
  $ionicPopup.alert({
   title: 'Error',
   template: 'Las contraseñas no coinciden, por favor, revísalas'
 });
}
}

})

.controller('OtrosInteresesCtrl', function($scope,$state,$rootScope) { 
  $scope.arguments.otrosIntereses = [];

  $scope.addElement = function(){  
    $scope.arguments.otrosIntereses.push($scope.arguments.otroInteres);
    $scope.arguments.otroInteres = "";
  }

  $scope.removeElement = function(index){  
   $scope.arguments.otrosIntereses.splice(index,1);
 }

 $scope.enviarOtrosIntereses = function (){

   var userData = JSON.parse(localStorage.getItem("userData"));
   var key = JSON.parse(userData.data.user[0].user_keywords);
   for(i=0;i<$scope.arguments.otrosIntereses.length;i++){
           key.push($scope.arguments.otrosIntereses[i]);
   }

   var interesesJSON = JSON.stringify({"data": {"user_id": userData.data.user[0].user_id,"user": {"user_psw": userData.data.user[0].user_psw,"user_keywords":  JSON.stringify(key) }},"option": "User","action": "update","token":localStorage.getItem("token")});

   var encrypted = CryptoJS.AES.encrypt(interesesJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
   var encryptedText = encrypted.toString();

   var settings = {
    "async": false,
    "crossDomain": true,
    "url": "http://goraymidev.softonthecloud.net/ws/",
    "method": "POST",
    "headers": {   
      "content-type": "application/x-www-form-urlencoded"
    },
    "data": {
      "data": encryptedText
    }
  }

  $.ajax(settings).done(function (response) {
    key = CryptoJS.enc.Utf8.parse('1234567891234567');  
    ciphertext = response; 
    var options = {mode: CryptoJS.mode.ECB};  
    plaintext = CryptoJS.AES.decrypt(ciphertext, key,options); 
    var data = CryptoJS.enc.Utf8.stringify(plaintext);  


    data = data.replace(/\\n/g, "\\n")  
    .replace(/\\'/g, "\\'")
    .replace(/\\"/g, '\\"')
    .replace(/\\&/g, "\\&")
    .replace(/\\r/g, "\\r")
    .replace(/\\t/g, "\\t")
    .replace(/\\b/g, "\\b")
    .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
data = data.replace(/[\u0000-\u0019]+/g,""); 
var o = JSON.parse(data);   

if(o.Error ===0){
   $rootScope.app.user = o;
   localStorage.setItem("userData", JSON.stringify($rootScope.app.user));
  $state.go('app.info');
  }

})
}
})
.controller('InteresesCtrl', function($scope,$rootScope,$filter,$state) {   
  $scope.arguments.intereses = [];

  var userData = JSON.parse(localStorage.getItem("userData"));

  var interesesJSON = JSON.stringify({"data": {"user_id": userData.data.user[0].user_id,"user_language": userData.data.user[0].user_default_language},"option": "Keywords","action": "getMainKeywords","token":localStorage.getItem("token")});

  var encrypted = CryptoJS.AES.encrypt(interesesJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
  var encryptedText = encrypted.toString();

  var settings = {
    "async": false,
    "crossDomain": true,
    "url": "http://goraymidev.softonthecloud.net/ws/",
    "method": "POST",
    "headers": {   
      "content-type": "application/x-www-form-urlencoded"
    },
    "data": {
      "data": encryptedText
    }
  }

  $.ajax(settings).done(function (response) {
    key = CryptoJS.enc.Utf8.parse('1234567891234567');  
    ciphertext = response; 
    var options = {mode: CryptoJS.mode.ECB};  
    plaintext = CryptoJS.AES.decrypt(ciphertext, key,options); 
    var data = CryptoJS.enc.Utf8.stringify(plaintext);  


    data = data.replace(/\\n/g, "\\n")  
    .replace(/\\'/g, "\\'")
    .replace(/\\"/g, '\\"')
    .replace(/\\&/g, "\\&")
    .replace(/\\r/g, "\\r")
    .replace(/\\t/g, "\\t")
    .replace(/\\b/g, "\\b")
    .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
data = data.replace(/[\u0000-\u0019]+/g,""); 
var o = JSON.parse(data);   

var showIntereses = $filter('filter')(o.data.keywords, { keyword_main_menu: "0" }); 

$rootScope.app.intereses = showIntereses;

//console.log($rootScope.app.intereses);     


$scope.anyadirIntereses = function(id,checked){

  if(!checked){
    var index = $scope.arguments.intereses.indexOf(id);
    $scope.arguments.intereses.splice(index,1)
  }else{

    $scope.arguments.intereses.push(id);
  }

};

$scope.enviarIntereses = function (){

 var userData = JSON.parse(localStorage.getItem("userData"));


 var interesesJSON = JSON.stringify({"data": {"user_id": userData.data.user[0].user_id,"user": {"user_psw": userData.data.user[0].user_psw,"user_keywords":  JSON.stringify($scope.arguments.intereses) }},"option": "User","action": "update","token":localStorage.getItem("token")});

 var encrypted = CryptoJS.AES.encrypt(interesesJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
 var encryptedText = encrypted.toString();

 var settings = {
  "async": false,
  "crossDomain": true,
  "url": "http://goraymidev.softonthecloud.net/ws/",
  "method": "POST",
  "headers": {   
    "content-type": "application/x-www-form-urlencoded"
  },
  "data": {
    "data": encryptedText
  }
}

$.ajax(settings).done(function (response) {
  key = CryptoJS.enc.Utf8.parse('1234567891234567');  
  ciphertext = response; 
  var options = {mode: CryptoJS.mode.ECB};  
  plaintext = CryptoJS.AES.decrypt(ciphertext, key,options); 
  var data = CryptoJS.enc.Utf8.stringify(plaintext);  


  data = data.replace(/\\n/g, "\\n")  
  .replace(/\\'/g, "\\'")
  .replace(/\\"/g, '\\"')
  .replace(/\\&/g, "\\&")
  .replace(/\\r/g, "\\r")
  .replace(/\\t/g, "\\t")
  .replace(/\\b/g, "\\b")
  .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
data = data.replace(/[\u0000-\u0019]+/g,""); 
var o = JSON.parse(data);   

//console.log(o);
if(o.Error ===0){
   $rootScope.app.user = o;
   localStorage.setItem("userData", JSON.stringify($rootScope.app.user));
  $state.go('app.otrosIntereses');
  }

})
}
})
})
.controller('GraciasCtrl', function($scope,$state) {   
})
.controller('LocalizarCtrl', function($scope, $cordovaGeolocation, $ionicLoading, $ionicPopup,$state,$rootScope) { 

  $ionicLoading.show({
    template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Buscando'
  });
  $scope.getCity = function(lat, lng){
   geocoder = new google.maps.Geocoder();
   var latlng = new google.maps.LatLng(lat, lng);
   geocoder.geocode({'latLng': latlng}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {     
      if (results[1]) {  
       $scope.$apply(function () {
           // $scope.arguments.localizacion= results[4].formatted_address;   
           $scope.localizacion = results[5].address_components[0].long_name;
           $scope.latlng = [lat,lng];
           localStorage.setItem("localizacion", $scope.localizacion);
         });      

     }
   }

 });
 }


 var posOptions = {enableHighAccuracy: true,timeout: 30000,maximumAge: 0};

 $cordovaGeolocation
 .getCurrentPosition(posOptions)
 .then(function (position) {
   var lat  = position.coords.latitude
   var long = position.coords.longitude
         // alert(lat + " --- " + long);      
         $scope.arguments.localizacion =  $scope.getCity(lat,long);

         setTimeout(function(){}, 5000);
         $ionicLoading.hide(); 
       }, function(err) {
        $ionicLoading.hide();        
        $ionicPopup.alert({
         title: 'Error',
         template: '¡Vaya! No te hemos podido localizar, dinos en que ciudad estás'

       });
      });   


 $scope.anyadirLocalizacion = function(){

   var userData = JSON.parse(localStorage.getItem("userData"));
   console.log(userData);

     //console.log(userData.data);


     var localizacionJSON = JSON.stringify({"data": {"user_id": userData.data.user[0].user_id,"user": {"user_psw": userData.data.user[0].user_psw,"user_default_location_lat": $scope.latlng[0] || 0,"user_default_location_lon": $scope.latlng[1] ||0,"user_default_location_name":$scope.localizacion }},"option": "User","action": "update","token":localStorage.getItem("token")});

     var encrypted = CryptoJS.AES.encrypt(localizacionJSON, CryptoJS.enc.Utf8.parse('1234567891234567'), {mode: CryptoJS.mode.ECB});
     var encryptedText = encrypted.toString();

     var settings = {
      "async": false,
      "crossDomain": true,
      "url": "http://goraymidev.softonthecloud.net/ws/",
      "method": "POST",
      "headers": {   
        "content-type": "application/x-www-form-urlencoded"
      },
      "data": {
        "data": encryptedText
      }
    }

    $.ajax(settings).done(function (response) {
      key = CryptoJS.enc.Utf8.parse('1234567891234567');  
      ciphertext = response; 
      var options = {mode: CryptoJS.mode.ECB};  
      plaintext = CryptoJS.AES.decrypt(ciphertext, key,options); 
      var data = CryptoJS.enc.Utf8.stringify(plaintext);  


      data = data.replace(/\\n/g, "\\n")  
      .replace(/\\'/g, "\\'")
      .replace(/\\"/g, '\\"')
      .replace(/\\&/g, "\\&")
      .replace(/\\r/g, "\\r")
      .replace(/\\t/g, "\\t")
      .replace(/\\b/g, "\\b")
      .replace(/\\f/g, "\\f");
// remove non-printable and other non-valid JSON chars
data = data.replace(/[\u0000-\u0019]+/g,""); 
var o = JSON.parse(data);  



if(o.Error ===0){
   $rootScope.app.user = o;
    localStorage.setItem("userData", JSON.stringify($rootScope.app.user));
  $state.go('app.intereses');
  }

});
  }; 
})
.controller('GaleriaCtrl', function($scope) { 

  $scope.items = [
  {
    src:'http://www.wired.com/images_blogs/rawfile/2013/11/offset_WaterHouseMarineImages_62652-2-660x440.jpg',
    sub: 'This is a <b>subtitle</b>'
  },
  {
    src:'http://www.gettyimages.co.uk/CMS/StaticContent/1391099215267_hero2.jpg',
    sub: '' /* Not showed */
  },
  {
    src:'http://www.hdwallpapersimages.com/wp-content/uploads/2014/01/Winter-Tiger-Wild-Cat-Images.jpg',
    thumb:'http://www.gettyimages.co.uk/CMS/StaticContent/1391099215267_hero2.jpg'
  }
  ]
})
.controller('DescubreCtrl', function($scope) {      

})
.controller('ComentarCtrl', function($scope) { 
  $scope.rating = 4;
  $scope.data = {
    rating : 1,
    max: 5
  }
  
  $scope.$watch('data.rating', function() {
  //console.log('New value: '+$scope.data.rating);
}); 
})
.controller('AprendeCtrl', function($scope) { 
})
.controller('CompraCtrl', function($scope) { 
})
.controller('ViveCtrl', function($scope) { 
})
.controller('DescubreCatCtrl', function($scope) { 
})
.controller('ViveCatCtrl', function($scope) { 
})
.controller('ViveFechaCtrl', function($scope) { 
})
.controller('ItemCtrl', function($scope,$ionicPopup) { 
  $scope.leerMas = function(){
     $ionicPopup.alert({
       title: 'Descripción',
       template: $scope.arguments.itemView.language_item_description  || "No hay más información"
     });

  }

})
.controller('ViveItemCtrl', function($scope) { 
})
.controller('RaymiCtrl', function($scope,$ionicPopup,$ionicSlideBoxDelegate,$timeout,$ionicLoading,$state) {   
  $ionicLoading.show({
    template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Cargando contenido'});
  $timeout(function(){
  $ionicLoading.show({
    template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Cargando contenido'});      
        $ionicSlideBoxDelegate.update();
          $ionicLoading.hide();
    },2000);
   $scope.leerMasRaymi = function(id){
    var alert = $ionicPopup.alert({
       title: 'Descripción',
       template: $scope.data.raymiView[id].language_item_description  || "No hay más información"
     });

     alert.then(function(res) {
     console.log('Thank you for not eating my delicious ice cream cone');
   });
  };
})
.controller('MapaCtrl', function($scope, $ionicLoading, $compile,$rootScope) {
  $scope.init = function() {
    var myLatlng = new google.maps.LatLng(-2.902656899999999,-79.00248640000001);
    if($scope.arguments.itemView){
      var myLatlng = new google.maps.LatLng( $scope.arguments.itemView.item_latitude,$scope.arguments.itemView.item_longitude); 

       var infowindow = new google.maps.InfoWindow({
          content: $scope.arguments.itemView.language_item_title
        });

      if($scope.arguments.itemView.item_tags.indexOf(",5,") > -1){
            markerDescubre = "img/map_descubre.png";
      }  
      else if($scope.arguments.itemView.item_tags.indexOf(",6,") > -1){
            markerDescubre = "img/map_aprende.png";
      }
       else if($scope.arguments.itemView.item_tags.indexOf(",7,") > -1){
            markerDescubre = "img/map_vive.png";
      }
      else{
            markerDescubre = "img/map_compra.png";
      }

}

    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"),
      mapOptions);

       

       

        var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          //title: 'Uluru (Ayers Rock)',
          icon : markerDescubre
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.open(map,marker);
        });

        $scope.map = map;
      };

    // google.maps.event.addDomListener(window, 'load', initialize);

    $scope.centerOnMe = function() {
      if(!$scope.map) {
        return;
      }

      $scope.loading = $ionicLoading.show({
        content: 'Getting current location...',
        showBackdrop: false
      });

      navigator.geolocation.getCurrentPosition(function(pos) {
        $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
        $scope.loading.hide();
      }, function(error) {
        alert('Unable to get location: ' + error.message);
      });
    };

    $scope.clickTest = function() {
      alert('Example of infowindow with ng-click')
    };


   $scope.filtroMapa = function(){
    var markers = [];
    var mapOptions = {
            center: new google.maps.LatLng(-2.902656899999999,-79.00248640000001),
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };

    var map = new google.maps.Map(document.getElementById("map"),
            mapOptions);
    var j=0;

  var myLatlng = new google.maps.LatLng(-2.902656899999999,-79.00248640000001);
  for(i=0;i<$rootScope.data.filtro.items.buy.length;i++){
       var myLatlng = new google.maps.LatLng($rootScope.data.filtro.items.buy[i].item_latitude,$rootScope.data.filtro.items.buy[i].item_longitude); 

      

          markers[j] = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: $rootScope.data.filtro.items.buy[i].language_item_title,
            icon :  "img/map_compra.png"
          });

          var infowindow = new google.maps.InfoWindow({
          content: $rootScope.data.filtro.items.buy[i].language_item_title
        });

          google.maps.event.addListener(markers[j], 'click', function() {
            infowindow.open(map,markers[j]);
          });
          j++;
  }
    for(i=0;i<$rootScope.data.filtro.items.discover.length;i++){
       var myLatlng = new google.maps.LatLng($rootScope.data.filtro.items.discover[i].item_latitude,$rootScope.data.filtro.items.discover[i].item_longitude);       

          markers[j] = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: $rootScope.data.filtro.items.discover[i].language_item_title,
            icon :  "img/map_descubre.png"
          });

          var infowindow = new google.maps.InfoWindow({
          content: $rootScope.data.filtro.items.discover[i].language_item_title
        });

          google.maps.event.addListener(markers[j], 'click', function() {
            infowindow.open(map,markers[j]);
          });
          j++;
  }
    for(i=0;i<$rootScope.data.filtro.items.learn.length;i++){
       var myLatlng = new google.maps.LatLng($rootScope.data.filtro.items.learn[i].item_latitude,$rootScope.data.filtro.items.learn[i].item_longitude);      

          markers[j] = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: $rootScope.data.filtro.items.learn[i].language_item_title,
            icon :  "img/map_aprende.png"
          });

          var infowindow = new google.maps.InfoWindow({
          content: $rootScope.data.filtro.items.learn[i].language_item_title
        });

          google.maps.event.addListener(markers[j], 'click', function() {
            infowindow.open(map,markers[j]);
          });
          j++;
  }
    for(i=0;i<$rootScope.data.filtro.items.live.length;i++){
       var myLatlng = new google.maps.LatLng($rootScope.data.filtro.items.live[i].item_latitude,$rootScope.data.filtro.items.live[i].item_longitude);      

          markers[j] = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: $rootScope.data.filtro.items.live[i].language_item_title,
            icon :  "img/map_vive.png"
          });

          var infowindow = new google.maps.InfoWindow({
          content: $rootScope.data.filtro.items.live[i].language_item_title
        });

          google.maps.event.addListener(markers[j], 'click', function() {
            infowindow.open(map,markers[j]);
          });
          j++;
  }

          $scope.map = map;
    };

    $scope.mapaRaymi = function(){
       var markers = [];
        var mapOptions = {
                center: new google.maps.LatLng(-2.902656899999999,-79.00248640000001),
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              };

        var map = new google.maps.Map(document.getElementById("map"),
                mapOptions);      

        var marker = "img/map_compra.png";

        if($scope.data.raymiView[0].item_tags.indexOf(",5,") > -1){
                  marker = "img/map_descubre.png";
            }  
       else if($scope.data.raymiView[0].item_tags.indexOf(",6,") > -1){
                  marker = "img/map_aprende.png";
            }
       else if($scope.data.raymiView[0].item_tags.indexOf(",7,") > -1){
                  marker = "img/map_vive.png";
            }
            else{
                  marker = "img/map_compra.png";
            }
      var myLatlng = new google.maps.LatLng(-2.902656899999999,-79.00248640000001);

       for(i=0;i<$scope.data.raymiView.length;i++){
       var myLatlng = new google.maps.LatLng($scope.data.raymiView[i].item_latitude,$scope.data.raymiView[i].item_longitude);       

          markers[i] = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: $scope.data.raymiView[i].language_item_title,
            icon :  marker
          });

          var infowindow = new google.maps.InfoWindow({
          content: $scope.data.raymiView[i].language_item_title
        });

          google.maps.event.addListener(markers[i], 'click', function() {
            infowindow.open(map,markers[i]);
          }); 

          map.setCenter(new google.maps.LatLng($scope.data.raymiView[i].item_latitude,$scope.data.raymiView[i].item_longitude));
         
        }
            $scope.map = map;



    };

  })

.controller('AyudanosCtrl', function($scope) {
  $scope.class = "button-royal";
  $scope.class2 =  "button-unactived";
  $scope.class3 =  "button-unactived";
  $scope.class4 = "button-royal";
  $scope.class5 =  "button-unactived";
  $scope.class6 =  "button-unactived";
  $scope.class7 = "button-royal";
  $scope.class8 =  "button-unactived";
  $scope.class9 =  "button-unactived";
  $scope.class10 = "button-royal";
  $scope.class11 =  "button-unactived";
  $scope.class12 =  "button-unactived";
  $scope.class13 = "button-royal";
  $scope.class14 =  "button-unactived";
  $scope.class15 =  "button-unactived";


  $scope.selectWifi = function (value){
    if(value == "si"){
     $scope.class = "button-royal";
     $scope.class2 =  "button-unactived";
     $scope.class3 =  "button-unactived";
   } else if (value == "no"){

     $scope.class =  "button-unactived";
     $scope.class2 =  "button-royal";
     $scope.class3 =  "button-unactived";
   }
   else {
     $scope.class =  "button-unactived";
     $scope.class2 =  "button-unactived" ;
     $scope.class3 =  "button-royal";
   }  

   $scope.wifi = value;
 };

 $scope.selectGastronomia = function (value){
  if(value == "si"){
   $scope.class4 = "button-royal";
   $scope.class5 =  "button-unactived";
   $scope.class6 =  "button-unactived";
 } else if (value == "no"){

   $scope.class4 =  "button-unactived";
   $scope.class5 =  "button-royal";
   $scope.class6  =  "button-unactived";
 }
 else {
   $scope.class4 =  "button-unactived";
   $scope.class5  =  "button-unactived" ;
   $scope.class6 =  "button-royal";
 }  

 $scope.gastronomia = value;
};

$scope.selectParque = function (value){
  if(value == "si"){
   $scope.class7 = "button-royal";
   $scope.class8 =  "button-unactived";
   $scope.class9 =  "button-unactived";
 } else if (value == "no"){

   $scope.class7 =  "button-unactived";
   $scope.class8 =  "button-royal";
   $scope.class9  =  "button-unactived";
 }
 else {
   $scope.class7 =  "button-unactived";
   $scope.class8  =  "button-unactived" ;
   $scope.class9 =  "button-royal";
 }  
 $scope.parque = value;
};

$scope.selectAcceso = function (value){
  if(value == "si"){
   $scope.class10 = "button-royal";
   $scope.class11 =  "button-unactived";
   $scope.class12 =  "button-unactived";
 } else if (value == "no"){

   $scope.class10 =  "button-unactived";
   $scope.class11 =  "button-royal";
   $scope.class12  =  "button-unactived";
 }
 else {
   $scope.class10 =  "button-unactived";
   $scope.class11  =  "button-unactived" ;
   $scope.class12 =  "button-royal";
 }  
 $scope.acceso = value;
};

$scope.selectJuegos = function (value){
  if(value == "si"){
   $scope.class13 = "button-royal";
   $scope.class14 =  "button-unactived";
   $scope.class15 =  "button-unactived";
 } else if (value == "no"){

   $scope.class13 =  "button-unactived";
   $scope.class14 =  "button-royal";
   $scope.class15  =  "button-unactived";
 }
 else {
   $scope.class13 =  "button-unactived";
   $scope.class14  =  "button-unactived" ;
   $scope.class15 =  "button-royal";
 }  
 $scope.juegos = value;
};

})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})
.constant('ratingConfig', {
  max: 5,
  stateOn: null,
  stateOff: null
})
.controller('RatingController', function($scope, $attrs, ratingConfig) {
  var ngModelCtrl;
  ngModelCtrl = {
    $setViewValue: angular.noop
  };
  this.init = function(ngModelCtrl_) {
    var max, ratingStates;
    ngModelCtrl = ngModelCtrl_;
    ngModelCtrl.$render = this.render;
    this.stateOn = angular.isDefined($attrs.stateOn) ? $scope.$parent.$eval($attrs.stateOn) : ratingConfig.stateOn;
    this.stateOff = angular.isDefined($attrs.stateOff) ? $scope.$parent.$eval($attrs.stateOff) : ratingConfig.stateOff;
    max = angular.isDefined($attrs.max) ? $scope.$parent.$eval($attrs.max) : ratingConfig.max;
    ratingStates = angular.isDefined($attrs.ratingStates) ? $scope.$parent.$eval($attrs.ratingStates) : new Array(max);
    return $scope.range = this.buildTemplateObjects(ratingStates);
  };
  this.buildTemplateObjects = function(states) {
    var i, j, len, ref;
    ref = states.length;
    for (j = 0, len = ref.length; j < len; j++) {
      i = ref[j];
      states[i] = angular.extend({
        index: 1
      }, {
        stateOn: this.stateOn,
        stateOff: this.stateOff
      }, states[i]);
    }
    return states;
  };
  $scope.rate = function(value) {
    if (!$scope.readonly && value >= 0 && value <= $scope.range.length) {
      ngModelCtrl.$setViewValue(value);
      return ngModelCtrl.$render();
    }
  };
  $scope.reset = function() {
    $scope.value = ngModelCtrl.$viewValue;
    return $scope.onLeave();
  };
  $scope.enter = function(value) {
    if (!$scope.readonly) {
      $scope.value = value;
    }
    return $scope.onHover({
      value: value
    });
  };
  $scope.onKeydown = function(evt) {
    if (/(37|38|39|40)/.test(evt.which)) {
      evt.preventDefault();
      evt.stopPropagation();
      return $scope.rate($scope.value + (evt.which === 38 || evt.which === 39 ? {
        1: -1
      } : void 0));
    }
  };
  this.render = function() {
    return $scope.value = ngModelCtrl.$viewValue;
  };
  return this;
}).directive('rating', function() {
  return {
    restrict: 'EA',
    require: ['rating', 'ngModel'],
    scope: {
      readonly: '=?',
      onHover: '&',
      onLeave: '&'
    },
    controller: 'RatingController',
    template: '<ul class="rating" ng-mouseleave="reset()" ng-keydown="onKeydown($event)">' + '<li ng-repeat="r in range track by $index" ng-click="rate($index + 1)"><i class="icon" ng-class="$index < value && (r.stateOn || \'ion-ios-star\') || (r.stateOff || \'ion-ios-star-outline\')"></i></li>' + '</ul>',
    replace: true,
    link: function(scope, element, attrs, ctrls) {
      var ngModelCtrl, ratingCtrl;
      ratingCtrl = ctrls[0];
      ngModelCtrl = ctrls[1];
      if (ngModelCtrl) {
        return ratingCtrl.init(ngModelCtrl);
      }
    }
  };
});

